class orwell {
  file { '/opt/scan_ipmi.sh':
    ensure => present,
    source => 'puppet:///modules/orwell/scan_ipmi.sh',
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { '/opt/ipmi2prom.py':
    ensure => present,
    source => 'puppet:///modules/orwell/ipmi2prom.py',
    mode   => '0700',
    owner  => 'root',
    group  => 'root',
  }

  file { '/etc/ipmi_scan_targets':
    ensure => present,
    source => 'puppet:///modules/orwell/ipmi_scan_targets',
    mode   => '0755',
    owner  => 'root',
    group  => 'root',
  }

  package { [ 'dnsmasq',]:
            ensure => installed,
  }

  cron { 'scan ipmi interfaces':
    command => '/opt/scan_ipmi.sh',
    user    => 'root',
    minute  => '*/2',
  }

  file { '/etc/sysconfig/nftables.conf':
    content => "include \"/etc/nftables/lysator.nft\"\n",
    mode    => '0440',
    owner   => 'root',
    group   => 'root',
  }
  -> file { '/etc/nftables/lysator.nft':
    ensure => present,
    source => 'puppet:///modules/orwell/nftables.conf',
    mode   => '0440',
    owner  => 'root',
    group  => 'root',
  }
  ~> service { 'nftables':
    ensure => running,
    enable => true,
  }

  include orwell::enoc_pdu_snmp
  include orwell::snmp_exporter
}

class orwell::enoc_pdu_snmp {
  package { 'net-snmp-utils':
    ensure => installed,
  }
  package { 'python3':
    ensure => installed,
  }

  file { '/root/.snmp/':
    ensure => directory,
  }
  -> file { '/root/.snmp/mibs/':
    ensure => directory,
  }
  -> file { '/root/.snmp/mibs/ip2017.MIB':
    ensure => file,
    source => 'puppet:///modules/orwell/ip2017.MIB',
  }

  file { '/usr/local/sbin/enoc_pdu_snmp2node_exporter.py':
    ensure => file,
    source => 'puppet:///modules/orwell/enoc_pdu_snmp2node_exporter.py',
    mode   => '0500',
  }

  $hosts = [
    'pdu-r1-1.mgmt',
    'pdu-r1-2.mgmt',
    'pdu-r2-1.mgmt',
    'pdu-r2-2.mgmt',
    'pdu-r3-1.mgmt',
    'pdu-r3-2.mgmt',
    'pdu-r4-1.mgmt',
    'pdu-r4-2.mgmt',
    'pdu-r5-1.mgmt',
    'pdu-r5-2.mgmt',
    'pdu-r6-1.mgmt',
    'pdu-r6-2.mgmt',
    'pdu-r7-1.mgmt',
    'pdu-r7-2.mgmt',
    'pdu-r8-1.mgmt',
    'pdu-r8-2.mgmt',
    'pdu-r9-1.mgmt',
    'pdu-r9-2.mgmt',
    'pdu-r10-1.mgmt',
    'pdu-r10-2.mgmt',
    'pdu-r11-1.mgmt',
    'pdu-r11-2.mgmt',
  ]

  file { '/usr/local/etc/enoc_pdu_snmp2node_exporter.hosts':
    ensure  => file,
    content => ($hosts + ['']).join("\n")
  }

  $timer_content = @(EOS)
[Timer]
OnCalendar=*:*:0/10
EOS

  $service_content = @(EOS)
[Service]
User=root
Type=oneshot
ExecStart=/usr/local/sbin/enoc_pdu_snmp2node_exporter.py
EOS

  systemd::timer { 'enoc_pdu_snmp2node_exporter.timer':
    active          => true,
    enable          => true,
    timer_content   => $timer_content,
    service_content => $service_content,
  }
}

class orwell::snmp_exporter {
  $_gpg_keys = [
    'https://raw.githubusercontent.com/lest/prometheus-rpm/master/RPM-GPG-KEY-prometheus-rpm',
  ]
  yumrepo { 'prometheus':
    ensure        => present,
    baseurl       => 'https://packagecloud.io/prometheus-rpm/release/el/$releasever/$basearch',
    enabled       => true,
    gpgcheck      => true,
    repo_gpgcheck => true,
    gpgkey        => $_gpg_keys.join("\n       "),
  }
  -> package { 'snmp_exporter': }
  -> file { '/etc/prometheus/snmp.yml':
    source => 'puppet:///modules/orwell/snmp.yml',
    mode   => '0640',
    owner  => 'root',
    group  => 'prometheus',
  }
  ~> service { 'snmp_exporter':
    ensure => running,
    enable => true,
  }
}
