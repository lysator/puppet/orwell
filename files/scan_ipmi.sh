#!/bin/bash

PROMETHEUS_DUMP_FILE=/var/lib/prometheus-dropzone/ipmi.prom
PROMETHEUS_TMP_FILE=/var/lib/prometheus-dropzone/ipmi.prom.tmp

for TARGET in $(cat /etc/ipmi_scan_targets)
do
		ipmitool -H $TARGET -P hunter2 -U ADMIN sensor | \
				/opt/ipmi2prom.py $TARGET >> $PROMETHEUS_TMP_FILE;
done
mv $PROMETHEUS_TMP_FILE $PROMETHEUS_DUMP_FILE
