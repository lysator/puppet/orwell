#!/usr/bin/python
import sys

if len(sys.argv) != 2:
    print "usage: ipmi2prom.py <hostname>"

hostname = sys.argv[1]

count = {}

for line in sys.stdin:
    data = line.split('|')
    key = data[0].strip().replace(" ","_").replace(".","_").lower()


    try:
        value = float(data[1])
    except:
        continue

    if key in count:
        count[key] += 1
    else:
        count[key] = 0

    print "ipmi_sensor{host=\"%s\",sensor_id=\"%s\",sensor=\"%s\"} %s" % (hostname,count[key],key,value)
