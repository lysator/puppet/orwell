#!/usr/bin/env python3
import ast
import os
import subprocess
from pathlib import Path


def process_kv(k, v):
    if v.startswith('"') and v.endswith('"'):
        return k, ast.literal_eval(v)
    elif 'Current' in k or 'Energy' in k:
        # The energy and current values received from the PDUs are multiplied by 10
        return k, int(v)/10
    return k, int(v)


def walk(agent):
    p = subprocess.run(
        [
            'snmpwalk', '-O', 'Q',
            '-r', '1',
            '-t', '1',
            '-m', 'IPPDU-MIB',
            '-v', '1',
            '-c', 'public',
            agent,
            '1.3.6.1.4.1.30966.10.3'
        ],
        stdout=subprocess.PIPE, stderr=subprocess.DEVNULL, encoding='utf-8'
    )
    data = [process_kv(*line.split(' = ', 1)) for line in p.stdout.strip().splitlines() if '=' in line]
    return {k: v for k, v in data}


def get_metrics(agent):
    d = walk(agent)

    lines = []

    for phase in ['A', 'B', 'C']:
        labels = {'phase': phase, 'agent': agent}
        labels_str = '{' + ','.join(f'{k}="{v}"' for k, v in labels.items()) + '}'

        try:
            voltage = d[f'IPPDU-MIB::mVoltage{phase}.0']
            lines.append(f'pdu_voltage{labels_str} {voltage}')
        except KeyError:
            pass

        try:
            current = d[f'IPPDU-MIB::mCurrent{phase}.0']
            lines.append(f'pdu_current{labels_str} {current}')
        except KeyError:
            pass

        try:
            # convert KWh to Joules, the preferred unit in prometheus
            energy = d[f'IPPDU-MIB::mEnergy{phase}.0'] * 3_600_000
            lines.append(f'pdu_joules_total{labels_str} {energy}')
        except KeyError:
            pass

    return lines


def main():
    with open('/usr/local/etc/enoc_pdu_snmp2node_exporter.hosts', 'r') as f:
        hosts = f.read().strip().splitlines()

    tmp_file = Path(f'/var/lib/prometheus-dropzone/pdu.prom.{os.getpid()}')

    with tmp_file.open('w') as f:
        for h in hosts:
            f.write('\n'.join(get_metrics(h)) + '\n')

    tmp_file.rename(tmp_file.with_name('pdu.prom'))


if __name__ == '__main__':
    main()
